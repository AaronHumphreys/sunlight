<?php namespace Klyp\Hummingbird;

use Cms\Classes\Theme;
use Backend;
use System\Classes\PluginBase;

/**
 * Hummingbird Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Hummingbird',
            'description' => 'Provides the main functionality for the Hummingbird theme',
            'author'      => 'Klyp',
            'icon'        => 'icon-paperclip'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Klyp\Hummingbird\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'klyp.hummingbird.some_permission' => [
                'tab' => 'Hummingbird',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'hummingbird' => [
                'label'       => 'Hummingbird',
                'iconSvg'     => '/plugins/klyp/hummingbird/assets/img/icon.svg',
                'url'         => Backend::url('klyp/hummingbird/components'),
                'permissions' => ['klyp.hummingbird.*'],
                'order'       => 200
            ],
        ];
    }

    /**
     * Register Twig markup tags.
     *
     * @return array
     */
    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'asset_path' => [&$this, 'webpackAsset'],
            ],
            'filters' => [
                'tel'             => [$this, 'formatPhone'],
                'cleanTruncate'   => [$this, 'cleanTruncate'],
                'faIcon'          => [$this, 'faIcon']
            ],
        ];
    }

    /**
     * Return versioned webpack asset path.
     *
     * @param string $path The file path we wish to find
     *
     * @return string
     */
    public function webpackAsset($path)
    {
        $manifestPath = themes_path(sprintf('%s/dist/manifest.json', Theme::getActiveTheme()->getDirName()));
        $manifest = file_exists($manifestPath) ? json_decode(file_get_contents($manifestPath), true) : [];

        return config('cms.themesPath') .'/'. Theme::getActiveTheme()->getDirName() .'/dist/'. $manifest[$path] ?? $path;
    }

    /**
     * Format phone number for use in tel links
     *
     * @param string $number The phone number to format
     *
     * @return string
     */
    public function formatPhone($number)
    {
        return preg_replace('/[()\s-]+/', '', $number);
    }

    /**
     * Clean and truncate a string to the given length
     *
     * @param string  $string The string to parse
     * @param integer $length The number of characters to truncate at
     *
     * @return string The clean string, stripped of tags and truncated
     */
    public function cleanTruncate($string, $length)
    {
        return str_limit(strip_tags($string), $length);
    }

    public function faIcon($icon)
    {
        $path = getcwd() . config('cms.themesPath') .'/'. Theme::getActiveTheme()->getDirName() . '/vendor/fa/svgs/' . $icon . '.svg';
        return file_get_contents($path);
    }
}
