<?php namespace Klyp\Base\Updates;

use File;
use Seeder;
use Backend\Models\BrandSetting;
use Backend\Models\User;
use System\Models\File as FileModel;

class SeedSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brand = BrandSetting::firstOrCreate([]);
        // Create CMS brand settings
        $brand->update([
            'app_name'        => 'Klyp CMS',
            'app_tagline'     => 'Nothing is static',
            'primary_color'   => '#4d4d4f',
            'secondary_color' => '#93b537',
            'accent_color'    => '#d7499a',
            'menu_mode'       => 'inline',
            'custom_css'      => '',
        ]);

        // Copy Klyp logo
        $fileName = 'klyp-logo.png';
        $diskName = '56247f815a3d6731597809.png';
        $sourcePath = plugins_path('klyp/hummingbird/assets/img');
        $destinationPath = storage_path('app/uploads/public/562/47f/815');
        File::makeDirectory($destinationPath, 0755, true, true);
        File::copy("{$sourcePath}/{$fileName}", "{$destinationPath}/{$diskName}");

        // Create file
        $logo = FileModel::updateOrCreate(['id' => 1], [
            'disk_name'       => $diskName,
            'file_name'       => $fileName,
            'file_size'       => File::size("{$destinationPath}/{$diskName}"),
            'content_type'    => 'image/png',
            'field'           => 'logo',
            'attachment_id'   => $brand->id,
            'attachment_type' => BrandSetting::class,
            'is_public'       => 1,
            'sort_order'      => 1,
        ]);
    }
}
