const CopyWebpackPlugin = require('copy-webpack-plugin');
const glob = require('glob');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const isDevelopmentMode = process.env.NODE_ENV !== 'production';
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const path = require('path');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const WebpackAssetsManifest = require('webpack-assets-manifest');

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    main: [
      './scripts/main.js'
    ],
  },
  output: {
    path: path.resolve('./storage/themes/sunlight/dist'),
    filename: isDevelopmentMode ? '[name].js' : '[name].[hash].js',
  },
  resolve: {
    modules: [
      'src',
      'node_modules',
    ]
  },
  stats: {
    errors: isDevelopmentMode ? true : false,
    errorDetails: isDevelopmentMode ? true : false,
  },
  devtool: isDevelopmentMode ? 'cheap-module-source-map' : 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader?cacheDirectory'
        }
      },
      {
        // that's your main setup for compiling scss
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
                config: {
                    path: path.resolve(__dirname, 'postcss.config.js')
                }
            },
          },
          'sass-loader'
        ],
      }
    ]
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: isDevelopmentMode
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: isDevelopmentMode ?  '[name].css' : '[name].[hash].css',
      chunkFilename: isDevelopmentMode ? '[name].css' : '[name].[hash].css',
    }),
    new WebpackAssetsManifest(),
    new CopyWebpackPlugin([
      {
        context: __dirname + '/src/',
        from: 'fonts',
        to: path.resolve('./storage/themes/sunlight/dist/fonts')
      }
    ]),
    // new CopyWebpackPlugin([
    //   {
    //     context: __dirname + '/node_modules/@fortawesome/fontawesome-free',
    //     from: 'svgs',
    //     to: path.resolve('../storage/themes/sunlight/dist/vendor')
    //   }
    // ]),
    new ImageminPlugin({
      externalImages: {
        context: __dirname + '/src/',
        sources: glob.sync('src/svg/**/*.svg'),
        destination: path.resolve('./storage/themes/sunlight/dist')
      }
    }),
    new ImageminPlugin({
      externalImages: {
        context: __dirname + '/node_modules/@fortawesome/fontawesome-free',
        sources: glob.sync('node_modules/@fortawesome/fontawesome-free/svgs/**/*.svg'),
        destination: path.resolve('./storage/themes/sunlight/vendor/fa')
      }
    })
  ]
};
