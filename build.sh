composer install --no-dev &&
// Generate DB Details
// Copy Details into .env
// Set node version
npm install &&
npm run build &&
rm -rf node_modules src &&
// Serve in test envrionment
// Run checkup / security scripts
curl -H "Accept: text/plain" https://security.sensiolabs.org/check_lock -F lock=@./composer.lock > /build/sunlight/composer-date.txt
