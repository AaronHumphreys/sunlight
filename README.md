# Hummingbird

## Installation
1. Clone the repo
1. `cd` into the project
1. Run `composer install`
1. Complete the details in the `.env` file
1. Run `php artisan october:up`

### First install
If installing as part of a new project, you may wish to run `composer update` to ensure the latest versions are in use.

## Production
Please log into the backend, navigate to "Settings" > "Log Settings" and enable "Log theme changes"

## Development
Currently utilising Node 8.11.4, the current LTS. Newer versions may work, but this will be tested and suppoerted on the LTS.

1. Ensure you have the correct node version setup
1. `cd` into `build`
1. Run `yarn` to install dependancies

### Commands
Run `yarn run <script>` to perform any of the following build scripts:

|Script|Description|
|--|--|
|dev|Compile assets rapidly for development|
|build|Compile and optimize the files for production|

## TODO
* Build Hummingbird theme 🐦
* Put default plugins into the repo